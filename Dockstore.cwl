description: "Whalesay deep quotes"
id: "dockstore-whalesay"
label: "dockstore whalesay bitbucket"

dct:creator:
  foaf:name: "Dockstore Test User"
cwlVersion: v1.0
class: CommandLineTool
baseCommand: echo
inputs:
  message:
    type: string
    inputBinding:
      position: 1
outputs: []
